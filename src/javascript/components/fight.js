import { controls } from '../../constants/controls';

export async function fight(firstFighter, secondFighter) {
  return new Promise((resolve) => {

  const firstFigtherHealthBar = document.getElementById("left-fighter-indicator");
  const secondFigtherHealthBar = document.getElementById("right-fighter-indicator");

  const firstFighterState = {
    ...firstFighter,
    fullHealth: firstFighter.health,
    block: false,
    availableCriticalHit: true,
  }

  const secondFighterState = {
    ...secondFighter,
    fullHealth: secondFighter.health,
    block: false,
    availableCriticalHit: true,
  }

  const keysPressed = {};

  document.addEventListener('keyup', e => {
    delete keysPressed[e.code];
    
    switch(e.code){
      case controls.PlayerOneBlock: {
        firstFighterState.block = false;
        break;
      }
      case controls.PlayerTwoBlock: {
        secondFighterState.block = false;
        break;
      }
    }
  });

  document.addEventListener('keydown', e => {
    keysPressed[e.code] = true;  
   
    switch(e.code){
      case controls.PlayerOneAttack: {
        if (!firstFighterState.block) hit(firstFighterState, secondFighterState);
        break;
      }
      case controls.PlayerTwoAttack: {
        if (!secondFighterState.block) hit(secondFighterState, firstFighterState);
        break;
      }
      case controls.PlayerOneBlock: {
        firstFighterState.block = true;
        break;
      }
      case controls.PlayerTwoBlock: {
        secondFighterState.block = true;
        break;
      }
    }
 
    const criticalAttackFirst = checkCritical(keysPressed, controls.PlayerOneCriticalHitCombination);
    const criticalAttackSecond = checkCritical(keysPressed, controls.PlayerTwoCriticalHitCombination);

    if (criticalAttackFirst && firstFighterState.availableCriticalHit){
      criticalHit(firstFighterState, secondFighterState);
    }

    if (criticalAttackSecond && secondFighterState.availableCriticalHit){
      criticalHit(secondFighterState, firstFighterState);
    }
    
    firstFigtherHealthBar.style.width = firstFighterState.health > 0 ? `${firstFighterState.health * 100 / firstFighterState.fullHealth}%` : '0%';
    secondFigtherHealthBar.style.width = secondFighterState.health > 0 ?`${secondFighterState.health * 100 /  secondFighterState.fullHealth}%` : '0%';

    if (firstFighterState.health <= 0) resolve(secondFighter)
    else if(secondFighterState.health <= 0) resolve(firstFighter);
  })

  function checkCritical(combinationObject, criticalHitCombination){
    const notCritical = criticalHitCombination.some(key => {
      if (!combinationObject[key]) return true;
      return false;
    });

    return !notCritical;
  }

  function hit(fighterOne, fighterTwo){
    if (!fighterTwo.block){
      fighterTwo.health -= getDamage(fighterOne, fighterTwo);
    }
  }

  function criticalHit(fighterOne, fighterTwo){
    fighterTwo.health -= fighterOne.attack * 2;
    fighterOne.availableCriticalHit = false;
    
    setTimeout(() => {
      fighterOne.availableCriticalHit = true;
    }, 10000);
  }
  });
}

export function getDamage(attacker, defender) {
  const defenderBlock = getBlockPower(defender);
  const attackerHit = getHitPower(attacker);
  return defenderBlock > attackerHit ? 0 : attackerHit - defenderBlock;
}

export function getHitPower(fighter) {
  const criticalHitChance = Math.random() + 1;
  return fighter.attack * criticalHitChance;
}

export function getBlockPower(fighter) {
  const dodgeChance = Math.random() + 1;
  return fighter.defense * dodgeChance;
}
