import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });

  
  if(fighter) {
    const attributes = { src: fighter.source };
    const fighterImage = createElement({
      tagName: 'img',
      className: `fighter-preview___image`,
      attributes
    });
    fighterElement.append(fighterImage);

    const fighterDescription = createElement({
      tagName: 'div',
      className: `fighter-preview___description`,
      attributes
    });
    fighterDescription.innerHTML  += `<p>
      Name: ${fighter.name} <br/>
      Health: ${fighter.health} <br/>
      Attack: ${fighter.attack} <br/>
      Defense: ${fighter.defense} <br/>
    </p>`;
    fighterElement.append(fighterDescription);
  }

  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = { 
    src: source, 
    title: name,
    alt: name 
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}
