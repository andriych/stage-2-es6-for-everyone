import { showModal } from "./modal";

export function showWinnerModal(fighter) {
  showModal({
    bodyElement: fighter.name,
    title: `You win, congrats!!!`
  })
}
